///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///
/// @author  Caleb Mueller <mc61@hawaii.edu>
/// @date    22_JAN_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define DEFAULT_MAX_NUMBER (2048)
#define FILENAME ("ascii_cat.txt")

int init_max(char *newMax)
{
   int userSetMax;
   if (newMax == NULL)
   {
      userSetMax = DEFAULT_MAX_NUMBER;
   }
   else
   {
      userSetMax = atoi(newMax);
   }
   return userSetMax;
}

int generateRandomInRange(int maxNum)
{
   return rand() % (maxNum + 1);
}

int getGuess(int maxNum)
{
   printf("OK cat, I'm thinking of a number from 1 to %d.  Make a guess:  ", maxNum);
   int aGuess;
   // Grab user input
   char buf[10];
   fgets(buf, sizeof(buf), stdin);
   aGuess = atoi(buf);
   // Make sure user input is valid and in range of the game
   if (!isdigit(*buf) || aGuess < 0 || aGuess > maxNum)
   {
      puts("Invalid input!\n");
      return -1;
   }
   return aGuess;
}

void asciiCat()
{
   int maxlength = 1024;
   FILE *fp = fopen(FILENAME, "r");
   char buffer[maxlength];

   while (fgets(buffer, maxlength, fp))
   {
      printf("%s", buffer);
   }
   fclose(fp);
}

///////////////////////////////////////////////////////////////
//////////////// START OF MAIN ////////////////////////////////
///////////////////////////////////////////////////////////////
int main(int argc, char *argv[])
{
   printf("Enter 0 at anytime to quit...\n");
   int maxNum = init_max(argv[1]);
   int guessToWin = generateRandomInRange(maxNum);

   do
   {
      int guess = getGuess(maxNum);
      //check to see if user input was valid
      //if invalid, exit loop with exit code of 1
      if (guess == -1)
      {
         return 1;
      }
      // If guess was valid game on!
      // Allow Cats that quit to quit
      if (guess == 0)
      {
         printf("Quitter!\n");
         break;
      }
      else if (guess < guessToWin)
      {
         //printf("No cat... the number I\'m thinking of is larger than %d\n", guess);
         printf("No cat... the number I\'m thinking of is\033[0;34m larger\033[0m than %d\n", guess);
         continue;
      }
      else if (guess > guessToWin)
      {
         printf("No cat... the number I\'m thinking of is\033[1;31m smaller\033[0m than %d\n", guess);
         continue;
      }
      else if (guess == guessToWin)
      {
         printf("You got me...\n");
         asciiCat();
         break;
      }
   } while (1);

   return 0;
}
